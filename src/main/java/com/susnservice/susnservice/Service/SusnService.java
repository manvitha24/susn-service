package com.susnservice.susnservice.Service;

import com.susnservice.susnservice.Model.SignUpResponse;
import com.susnservice.susnservice.Model.SignUpResquest;
import com.susnservice.susnservice.RestTemplateService.RestTemplateService;
import com.susnservice.susnservice.Validation.ValidateRequests;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SusnService {

    @Autowired
    ValidateRequests validateRequests;

    @Autowired
    RestTemplateService restTemplateService;

    public SignUpResponse userSignUp(String txRoot, String SystemId, SignUpResquest signUpResquest){
        validateRequests.validateSignUp(signUpResquest);
        ResponseEntity<SignUpResponse> response = restTemplateService.signUp(txRoot, SystemId, signUpResquest);
        SignUpResponse signUpResponse = response.getBody();
        return signUpResponse;
    }
}
