package com.susnservice.susnservice.Exception;


public class UserException extends RuntimeException {

    public UserException(String message){
        super(message);
    }
}
