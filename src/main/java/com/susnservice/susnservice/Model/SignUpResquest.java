package com.susnservice.susnservice.Model;

import lombok.Data;

@Data
public class SignUpResquest {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String password;
    private String address;
    private String university;
    private String designation;
    private String team;

}
