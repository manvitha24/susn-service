package com.susnservice.susnservice.Validation;

import com.susnservice.susnservice.Exception.UserException;
import com.susnservice.susnservice.Model.SignUpResquest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ValidateRequests {

    public void validateSignUp(SignUpResquest signUpResquest){
        if(StringUtils.isEmpty(signUpResquest.getFirstName())){
            throw new UserException("First Name cannot be empty");
        }
        if(StringUtils.isEmpty(signUpResquest.getLastName())){
            throw new UserException("Last Name cannot be empty");
        }
        if(StringUtils.isEmpty(signUpResquest.getEmail())){
            throw new UserException("Email cannot be empty");
        }
        if(StringUtils.isEmpty(signUpResquest.getPhoneNumber())){
            throw new UserException("Phone number cannot be empty");
        }
        if(StringUtils.isEmpty(signUpResquest.getUniversity())){
            throw new UserException("University cannot be empty");
        }
        if(StringUtils.isEmpty(signUpResquest.getAddress())){
            throw new UserException("Address cannot be empty");
        }
    }
}
